from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
import requests

from common.json import ModelEncoder
from .models import (
  Sale,
  Salesperson,
  Customer,
  AutomobileVO
)

class AutoVODetailEncoder(ModelEncoder):
  model = AutomobileVO
  properties = [
    "import_href",
    "color",
    "year",
    "vin",
    "model",
    "sold",
    "picture_url",
    "manufacturer",
  ]
  def default(self, o):
    if isinstance(o, AutomobileVO):
      return {
        "import_href": o.import_href,
        "color": o.color,
        "year": o.year,
        "vin": o.vin,
        "model": o.model,
        "sold": o.sold,
        "picture_url": o.picture_url,
        "manufacturer": o.manufacturer,
      }
    return super().default(o)

class SalespersonDetailEncoder(ModelEncoder):
  model = Salesperson
  properties = [
    "first_name",
    "last_name",
    "employee_id",
  ]

class CustomerDetailEncoder(ModelEncoder):
  model = Customer
  properties = [
    "id",
    "first_name",
    "last_name",
    "address",
    "phone_number",
  ]

class SalesDetailEncoder(ModelEncoder):
  model = Sale
  properties = [
    "id",
    "price",
    "salesperson",
    "customer",
    "automobile",
  ]
  encoders = {
    "automobile" : AutoVODetailEncoder(),
    "salesperson" : SalespersonDetailEncoder(),
    "customer" : CustomerDetailEncoder(),
  }

  def get_extra_data(self, o):
    automobile = o.automobile
    return {
        "import_href": automobile.import_href,
        "color": automobile.color,
        "year": automobile.year,
        "vin": automobile.vin,
        "model": automobile.model,
        "sold": automobile.sold,
        "picture_url": automobile.picture_url,
        "manufacturer": automobile.manufacturer,
    }

@require_http_methods(["GET", "POST"])
def api_list_salespersons(request):
  if request.method == "GET":
    salespersons = Salesperson.objects.all()
    return JsonResponse(
      {"salespersons": salespersons},
      encoder=SalespersonDetailEncoder,
    )

  else:
    content = json.loads(request.body)
    # print("POST data", content)
    salesperson = Salesperson.objects.create(**content)
    return JsonResponse(
      salesperson,
      encoder=SalespersonDetailEncoder,
      safe=False,
    )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_salesperson_details(request, pk):
  if request.method == "GET":
    try:
      salesperson = Salesperson.objects.get(employee_id=pk)

      return JsonResponse(
        salesperson,
        encoder=SalespersonDetailEncoder,
        safe=False,
      )

    except Salesperson.DoesNotExist:
      return JsonResponse(
        {"message": "Does not exist"},
      )

  elif request.method == "DELETE":
    try:
      salesperson = Salesperson.objects.get(employee_id=pk)
      salesperson.delete()

      return JsonResponse(
        salesperson,
        encoder=SalespersonDetailEncoder,
        safe=False,
      )

    except Salesperson.DoesNotExist:
      return JsonResponse(
        {"message": "Does not exist"},
      )

  else:
    content = json.loads(request.body)
    try:
      if "salesperson" in content:
        salesperson = Salesperson.objects.get(employee_id=pk)
        content["salesperson"] = salesperson

    except Salesperson.DoesNotExist:
      return JsonResponse(
        {"message": "Does not exist"},
      )

    Salesperson.objects.filter(employee_id=pk).update(**content)
    salesperson = Salesperson.objects.get(employee_id=pk)

    return JsonResponse(
      salesperson,
      encoder=SalespersonDetailEncoder,
      safe=False,
    )

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
  if request.method == "GET":
    customers = Customer.objects.all()
    return JsonResponse(
      {"customers": customers},
      encoder=CustomerDetailEncoder,
    )

  else:
    content = json.loads(request.body)
    if "phone_number" in content and len(content["phone_number"]) == 10 and content["phone_number"].isdigit():
      customers = Customer.objects.create(**content)
      return JsonResponse(
        customers,
        encoder=CustomerDetailEncoder,
        safe=False,
      )

    else:
      return JsonResponse(
        {"message": "Invalid phone number"}
      )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_customer_details(request, pk):
  if request.method == "GET":
    try:
      customer = Customer.objects.get(id=pk)

      return JsonResponse(
        customer,
        encoder=CustomerDetailEncoder,
        safe=False,
      )

    except Customer.DoesNotExist:
      return JsonResponse(
        {"message": "Does not exist"},
      )

  elif request.method == "DELETE":
    try:
      customer = Customer.objects.get(id=pk)
      customer.delete()

      return JsonResponse(
        customer,
        encoder=CustomerDetailEncoder,
        safe=False,
      )

    except Customer.DoesNotExist:
      return JsonResponse(
        {"message": "Does not exist"},
      )

  else:
    content = json.loads(request.body)
    try:
      if "customer" in content:
        customer = Customer.objects.get(id=pk)
        content["customer"] = customer

    except Customer.DoesNotExist:
      return JsonResponse(
        {"message": "Does not exist"},
        status=400,
      )

    Customer.objects.filter(id=pk).update(**content)
    customer = Customer.objects.get(id=pk)

    return JsonResponse(
      customer,
      encoder=CustomerDetailEncoder,
      safe=False,
    )

@require_http_methods(["GET", "POST"])
def api_list_sales(request):
  if request.method == "GET":
    sales = Sale.objects.all()
    return JsonResponse(
      {"sales": sales},
      encoder=SalesDetailEncoder,
    )

  else:
    content = json.loads(request.body)
    print("POST data", content)

    try:
      salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
      content["salesperson"] = salesperson

    except Salesperson.DoesNotExist:
      return JsonResponse(
        {"message": "salesperson does not exist"},
        status=400,
      )

    try:
      customer = Customer.objects.get(id=content["customer"])
      content["customer"] = customer

    except Customer.DoesNotExist:
      return JsonResponse(
        {"message": "customer does not exist"},
        status=400,
      )

    try:
      automobile_href = content["automobile"]
      print(automobile_href)
      automobile = AutomobileVO.objects.get(import_href=automobile_href)
      print(automobile)

      if automobile.sold:
        return JsonResponse(
          {"message": "Vehicle is already sold."},
        )

      content["automobile"] = automobile

    except AutomobileVO.DoesNotExist:
      print(content)
      return JsonResponse(
        {"message": "car does not exist"},
        status=400,
      )

    update_url = f"http://project-beta-inventory-api-1:8000/api/automobiles/{automobile.vin}/"
    headers = {"Content-Type": "application/json"}
    data = json.dumps({"sold": True})
    response = requests.put(update_url, headers=headers, data=data)

    automobile.sold = True
    automobile.save()
    sale = Sale.objects.create(**content)
    return JsonResponse(
      sale,
      encoder=SalesDetailEncoder,
      safe=False,
    )

@require_http_methods(["DELETE"])
def api_sale_details(request, pk):
  if request.method == "DELETE":
    try:
      sale = Sale.objects.get(id=pk)
      sale.delete()

      return JsonResponse(
        sale,
        encoder=SalesDetailEncoder,
        safe=False,
      )

    except Sale.DoesNotExist:
      return JsonResponse(
        {"message": "does not exist"},
      )
