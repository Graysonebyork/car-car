import {Link} from "react-router-dom";
import "./index.css";

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership management!
        </p>
        <div id="lightning-mcqueen-carousel" className="carousel slide" data-bs-ride="carousel" data-bs-interval="4000">
          <div className="carousel-inner">
            <div className="carousel-item active">
              <Link to="/list-automobile">
                <div className="overlay"></div>
                <img
                  src="https://upload.wikimedia.org/wikipedia/en/8/82/Lightning_McQueen.png"
                  className="d-block w-100"
                  alt="I AM SPEED"
                />
                <div className="carousel-caption">
                  <h5>Inventory</h5>
                  <p>See our partner manufacturers and our available models</p>
                </div>
              </Link>
            </div>
            <div className="carousel-item">
              <Link to ="/sales/">
                <div className="overlay"></div>
                <img
                  src="https://i.pinimg.com/736x/c4/50/07/c45007f8ec1d37926d87befde23ec323.jpg"
                  className="d-block w-100"
                  alt="I AM SPEED"
                />
                <div className="carousel-caption">
                  <h5>Sales</h5>
                  <p>Our recent sales. Be one of our happy customers!</p>
                </div>
              </Link>
            </div>
            <div className="carousel-item">
              <Link to="/create-appointment/">
                <div className="overlay"></div>
                <img
                  src="https://www.motionpictures.org/wp-content/uploads/2017/04/Cars358dd6f1c65330.jpg"
                  className="d-block w-100"
                  alt="I AM SPEED"
                />
                <div className="carousel-caption">
                  <h5>Services</h5>
                  <p>Set up a service appoointment today!</p>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
