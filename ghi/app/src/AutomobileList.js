import React, {useState, useEffect} from 'react';


const ListAutomobile = function() {
    const [automobiles, setAutomobiles] = useState([]);

    const getAutomobiles = async function() {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        const automobiles = await response.json();
        setAutomobiles(automobiles.autos);
    }

    useEffect(() => {
        getAutomobiles();
    }, []);

    return (
        <div className="text-center">
            <h2 className="p-3 mb-3">Registered Autos</h2>
            <table className="p-3 container">
                <thead className="mb-3 bg-success text-light">
                    <tr>
                        <th>Color</th>
                        <th>Year</th>
                        <th>VIN</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                {automobiles.map((automobile) => {
                    return <tbody key={automobile.id}>
                        <tr className="mb-3 p-1 border border-success bg-light">
                            <td>{automobile.color}</td>
                            <td>{automobile.year}</td>
                            <td>{automobile.vin}</td>
                            <td>{automobile.model.name}</td>
                            <td>{automobile.model.manufacturer.name}</td>
                            <td>{automobile.sold}</td>
                        </tr>
                    </tbody>
                })}
            </table>
        </div>
    );
}


export default ListAutomobile;
