import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListSales from './SaleList';
import SaleForm from './SaleForm';
import SPersonForm from './SPersonForm';
import SPersonTable from './SPersonTable';
import CustomerTable from './CustomerTable';
import CustomerForm from './CustomerForm';
import TechnicianList from './TechnicianList';
import AddTechnician from './AddTechnician';
import ServiceHistory from './ServiceHistory';
import CreateAppointment from './CreateAppointment';
import AppointmentList from './AppointmentList';
import ManufacturerCreate from './ManufacturerCreate';
import ManufacturerList from './ManufacturerList';
import VehicleModelCreate from './VehicleModelCreate';
import VehicleModelList from './VehicleModelList';
import CreateAutomobile from './AutomobileCreate';
import ListAutomobile from './AutomobileList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="sales">
            <Route index element={<ListSales />} />
            <Route path="new" element={<SaleForm />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SPersonTable />} />
            <Route path="new" element={<SPersonForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomerTable />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="/technicians/" element={<TechnicianList />} />
          <Route path="/technicians/add/" element={<AddTechnician />} />
          <Route path="/service-history/" element={<ServiceHistory />} />
          <Route path="/create-appointment/" element={<CreateAppointment />} />
          <Route path="/appointment-list/" element={<AppointmentList />} />
          <Route path="/create-manufacturer/" element={<ManufacturerCreate />} />
          <Route path="/list-manufacturer/" element={<ManufacturerList />} />
          <Route path="/create-vehicleModel/" element={<VehicleModelCreate />} />
          <Route path="/list-vehicleModel/" element={<VehicleModelList />} />
          <Route path="/create-automobile/" element={<CreateAutomobile />} />
          <Route path="/list-automobile/" element={<ListAutomobile />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
