import {useEffect, useState} from 'react';
import {Link} from "react-router-dom";

function CustomerTable() {
  const [customer, setCustomer] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/customers/');

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setCustomer(data.customers);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Address</th>
            <th>Phone Number</th>
          </tr>
        </thead>
        <tbody>
          {customer.map(customer => {
            return (
              <tr key={customer.id}>
                <td>{customer.first_name} {customer.last_name}</td>
                <td>{customer.address}</td>
                <td>{customer.phone_number}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Link to="/customers/new" className="btn btn-primary fixed-bottom">
        Add a Customer
      </Link>
    </div>
  );
}

export default CustomerTable;
