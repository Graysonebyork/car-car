import React, {useState, useEffect } from 'react';


const VehicleModelList = function(){
    const [models, setModels] = useState([]);

    const getModels = async function() {
        const url = "http://localhost:8100/api/models/"
        const response = await fetch(url);
        const modelsOb = await response.json();
        setModels(modelsOb.models);
    }

    useEffect(() => {
        getModels();
    }, []);

    return (

        <div className="p-3 mb-2 bg-light container text-center">
            <h2>Registered Models</h2>
            <div className="row border border-success-subtle bg-success text-light">
                <h3 className="col">Model</h3>
                <h3 className="col"> </h3>
                <h3 className="col">Manufacturer</h3>
            </div>
            <div>
                {models.map((model) => {
                    return <div className="row border border-success" key={model.id}>
                        <div className="col">{model.name}</div>
                        <img className="col mx-auto img-thumnail" src={model.picture_url} />
                        <div className="col">{model.manufacturer.name}</div>
                    </div>
                })};
            </div>
        </div>

    );
}


export default VehicleModelList;
