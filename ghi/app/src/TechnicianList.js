import React, { useState, useEffect } from 'react';


function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);

    const getTechnicians = async function() {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);
        console.log("response:", response);
        const techniciansOb = await response.json();
        const technicians = techniciansOb["technicians"];
        setTechnicians(technicians);
        console.log("is it an array?", Array.isArray(technicians), technicians);
    }

    useEffect(() => {
        getTechnicians();
    }, []);

    return (
      <div className="p-3 mb-2 bg-light">
        <div className="container text-center">
          <h2>Our Technicians</h2>
            <div>
              <div className="row border border-success-subtle rounded-4 bg-success text-light">
                <h3 className="col">ID</h3>
                <h3 className="col">Name</h3>
              </div>
            {technicians.map((technician)=> {
                return (<div key={technician.id} className="row border border-success rounded-pill">
                    <div className="col">{technician.employee_id}</div>
                    <div className="col">{technician.first_name} {technician.last_name}</div>
                 </div>
                )}
                )}
            </div>
        </div>
      </div>
    );
  }


  export default TechnicianList;
