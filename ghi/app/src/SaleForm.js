import React, {useState, useEffect} from "react";
import {useNavigate} from "react-router-dom";

function SaleForm() {
  const [automobile, setAutomobile] = useState([]);
  const [customer, setCustomer] = useState([]);
  const [salesperson, setSalesperson] = useState([]);
  const [formData, setFormData] = useState({
    price: "",
  });

  const nav = useNavigate();

  const getData = async () => {
    const autoUrl = "http://localhost:8100/api/automobiles/";
    const customerUrl = "http://localhost:8090/api/customers/";
    const sPersonUrl = "http://localhost:8090/api/salespeople/";

    // Use Promise.all() to wait until all fetch requests are complete
    // Thank you terrible fanfic. Thank you Lina
    try {
      const responses = await Promise.all([
        fetch(autoUrl),
        fetch(customerUrl),
        fetch(sPersonUrl),
      ]);

      const [autoData, customerData, sPersonData] = await Promise.all(
        responses.map(response => response.json())
      );
      console.log(autoData);
      console.log(customerData);
      console.log(sPersonData);

      setAutomobile(autoData.autos.filter(auto => {
        return auto.sold !== true; // Filter out sold cars
      }));
      setCustomer(customerData.customers);
      setSalesperson(sPersonData.salespersons);


      } catch (e) {
        console.error("Error getting data", e);
      }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8090/api/sales/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      }
    };

    const response = await fetch(url, fetchConfig);
    console.log("Response", response);

    if (response.ok) {
      setFormData({
        automobile: "",
        customer: "",
        salesperson: "",
        price: "",
      });
      nav("/sales");
    };
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record Your Transaction</h1>
          <form onSubmit={handleSubmit} id="add-sale-form">
            <div className="mb-3">
                <select onChange={handleFormChange}
                value={formData.automobile}
                required name="automobile"
                id="automobile"
                className="form-select"
                >
                  <option value="">Select the corresponding VIN for your vehicle</option>
                  {automobile.map(auto => {
                    return (
                      <option key={auto.vin} value={auto.href}>{auto.vin}</option>
                    )
                  })}
                </select>
              </div>
            <div className="mb-3">
                <select onChange={handleFormChange}
                value={formData.customer}
                required name="customer"
                id="customer"
                className="form-select"
                >
                  <option value="">Select Customer</option>
                  {customer.map(customer => {
                    return (
                      <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                    )
                  })}
                </select>
              </div>
            <div className="mb-3">
                <select onChange={handleFormChange}
                value={formData.salespersons}
                required name="salesperson"
                id="salesperson"
                className="form-select"
                >
                  <option value="">Select Salesperson</option>
                  {salesperson.map(salesperson => {
                    return (
                      <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                    )
                  })}
                </select>
              </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange}
              value={formData.price}
              placeholder="Price"
              required type="text"
              name="price"
              id="price"
              className="form-control"
              />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SaleForm;
