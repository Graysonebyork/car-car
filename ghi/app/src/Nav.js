import {NavLink} from 'react-router-dom';
import {useState} from 'react';


function Nav() {
  const [showSalesDropdown, setShowSalesDropdown] = useState(false);
  const [showEmployeesDropdown, setShowEmployeesDropdown] = useState(false);
  const [showCustomersDropdown, setShowCustomersDropdown] = useState(false);
  const [showServicesDropdown, setShowServicesDropdown] = useState(false);
  const [showInventoryDropdown, setShowInventoryDropdown] = useState(false);

  const handleClick = (dropdown) => {
    if (dropdown === "sales") {
      setShowSalesDropdown(!showSalesDropdown);
    } else if (dropdown === "employees") {
      setShowEmployeesDropdown(!showEmployeesDropdown);
    } else if (dropdown === "customers") {
      setShowCustomersDropdown(!showCustomersDropdown);
    } else if (dropdown === "services") {
      setShowServicesDropdown(!showServicesDropdown);
    } else if (dropdown === "inventory") {
      setShowInventoryDropdown(!showInventoryDropdown);
    }
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <div className="dropdown-toggle navbar-brand"
              id="navbarDropdown"
              role="button"
              data-bs-toggle="dropdown"
              onClick={() => handleClick("sales")}
              >Sales</div>
              <ul className={`dropdown-menu dropdown-menu-dark ${showSalesDropdown ? "show" : ""}`}>
                <li><NavLink className="dropdown-item" to="/sales/">Sales Records</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales/new">Record New Sale</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <div className="dropdown-toggle navbar-brand"
              id="navbarDropdown"
              role="button"
              data-bs-toggle="dropdown"
              onClick={() => handleClick("employees")}
              >Meet Our Team</div>
              <ul className={`dropdown-menu dropdown-menu-dark ${showEmployeesDropdown ? "show" : ""}`}>
                <li><NavLink className="dropdown-item" to="/salespeople/">Sales Team</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians/">Technicians</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople/new">New Salespeople</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians/add">New Technician</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <div className="dropdown-toggle navbar-brand"
              id="navbarDropdown"
              role="button"
              data-bs-toggle="dropdown"
              onClick={() => handleClick("customer")}
              >Customers</div>
              <ul className={`dropdown-menu dropdown-menu-dark ${showCustomersDropdown ? "show" : ""}`}>
                <li><NavLink className="dropdown-item" to="/customers/">Customer List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customers/new">New Customer</NavLink></li>
              </ul>
            </li>
          </ul>
         <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <div className="dropdown-toggle navbar-brand"
              id="navbarDropdown"
              role="button"
              data-bs-toggle="dropdown"
              onClick={() => handleClick("services")}
              >Service Center</div>
              <ul className={`dropdown-menu dropdown-menu-dark ${showServicesDropdown ? "show" : ""}`}>
                <li><NavLink className="dropdown-item" to="/appointment-list/">Appointments</NavLink></li>
                <li><NavLink className="dropdown-item" to="/create-appointment/">Schedule Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="/service-history/">Service Record</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
         <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <div className="dropdown-toggle navbar-brand"
              id="navbarDropdown"
              role="button"
              data-bs-toggle="dropdown"
              onClick={() => handleClick("inventory")}
              >Inventory</div>
              <ul className={`dropdown-menu dropdown-menu-dark ${showInventoryDropdown ? "show" : ""}`}>
                <li><NavLink className="dropdown-item" to="/list-manufacturer/">Manufacturers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/create-manufacturer/">Register a Manufacturer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/list-vehicleModel/">Available Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="/create-vehicleModel/">New Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="/list-automobile/">List of Automobiles</NavLink></li>
                <li><NavLink className="dropdown-item" to="/create-automobile/">New Vehicles!</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
        </div>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          </ul>
        </div>
      </div>
    </nav>
  );
}


export default Nav;
