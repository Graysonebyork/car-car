import React, { useState, useEffect } from 'react';


function AddTechnician() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const fetchData = async () => {
        console.log("why are we doing this?");
    }
        useEffect(() => {
            fetchData();
        }, [])

        const handleChangeFirstName = (event) => {
            const value = event.target.value;
            setFirstName(value);
        }

        const handleChangeLastName = (event) => {
            const value = event.target.value;
            setLastName(value);
        }

        const handleChangeEmployeeId = (event) => {
            const value = event.target.value;
            setEmployeeId(value);
        }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const technicianResponse = await fetch(technicianUrl, fetchOptions);
        if (technicianResponse.ok) {
            const newTechnician = await technicianResponse.json();

            setFirstName('');
            setLastName('');
            setEmployeeId('');
        }
        }

return (
    <div className="p-3 mb-2 bg-light text-emphasis-dark">
        <h1 className="mb-3">Join our Team!</h1>
        <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="row g-3">
                <div className="form-floating col-lg">
                    <input className="form-control form-control-lg mb-3" onChange={handleChangeFirstName} placeholder="first name" value={firstName} type="text" name="first_name" id="first_name" />
                    <label htmlFor="firstName">First Name</label>
                </div>
                <div className="form-floating col-lg">
                    <input className="form-control form-control-lg mb-3" onChange={handleChangeLastName} placeholder="last name" value={lastName} type="text" name="last_name" id="last_name" />
                    <label htmlFor="lastName">Last Name</label>
                </div>
            </div>
            <div className="input-group input-group-lg mb-3">
                <div className="form-floating col-lg">
                    <input className="form-control form-control-lg" onChange={handleChangeEmployeeId} placeholder="employee ID" value={employeeId} type="text" name="employee_id" id="employee_id" />
                    <label htmlFor="employeeId">Employee ID</label>
                </div>
                <div>
                    <button className="btn btn-lg btn-success" data-bs-toggle="modal" data-bs-target="#technicianSubmitModal" type="button">Create</button>
                    <div className="modal fade" id="technicianSubmitModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h1 className="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
                                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div className="modal-body">
                                    Welcome {firstName}, glad to have you with us!
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={handleSubmit}>Confirm</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
)
}


export default AddTechnician;
